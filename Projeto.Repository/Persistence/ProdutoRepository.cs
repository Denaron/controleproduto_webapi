﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projeto.Entities; //entidades..
using Projeto.Repository.Connections; //conexão..
using System.Data.Entity; //entityframework..

namespace Projeto.Repository.Persistence
{
    public class ProdutoRepository
    {
        public void Insert(Produto p)
        {
            using (Conexao con = new Conexao())
            {
                con.Entry(p).State = EntityState.Added; //inserindo..
                con.SaveChanges(); //executando..
            }
        }

        public void Update(Produto p)
        {
            using (Conexao con = new Conexao())
            {
                con.Entry(p).State = EntityState.Modified; //atualizando..
                con.SaveChanges(); //executando..
            }
        }

        public void Delete(Produto p)
        {
            using (Conexao con = new Conexao())
            {
                con.Entry(p).State = EntityState.Deleted; //excluindo..
                con.SaveChanges(); //executando..
            }
        }

        public List<Produto> FindAll()
        {
            using (Conexao con = new Conexao())
            {
                return con.Produto.ToList();
            }
        }

        public Produto FindById(int idProduto)
        {
            using (Conexao con = new Conexao())
            {
                return con.Produto.Find(idProduto);
            }
        }

    }
}
